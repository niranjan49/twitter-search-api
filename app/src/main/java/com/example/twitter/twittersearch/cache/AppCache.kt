package com.example.twitter.twittersearch.cache

import android.widget.ArrayAdapter
import com.example.twitter.twittersearch.model.Tweet
import com.example.twitter.twittersearch.model.TweetsList
import java.util.ArrayList

class AppCache {

    var token: String? = null
    var tweetList: ArrayList<Tweet>? = null

    companion object {
      private var cache: AppCache? = null

        fun getCache(): AppCache {
            if (cache == null) cache = AppCache()
            return cache as AppCache
        }

    }

}