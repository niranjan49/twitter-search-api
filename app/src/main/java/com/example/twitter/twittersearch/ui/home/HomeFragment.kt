package com.example.twitter.twittersearch.ui.home

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.support.v7.view.menu.MenuBuilder
import android.support.v7.view.menu.MenuPopupHelper
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.blooms.twidapp.cache.PrefManager
import com.blooms.twidapp.utils.DialogUtil
import com.example.twitter.twittersearch.R
import com.example.twitter.twittersearch.model.Tweet
import com.niranjan.videoplayer.base.BaseFragment
import com.niranjan.videoplayer.ui.home.adapters.FilterItemListAdapter

class HomeFragment : BaseFragment(), HomeView.View, View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.image_filter -> {
                filterMenu()
            }
        }
    }

    @SuppressLint("RestrictedApi")
    fun filterMenu() {
        val menuBuilder = MenuBuilder(activity)
        val inflater = MenuInflater(activity)
        inflater.inflate(R.menu.filter_options, menuBuilder)
        val optionsMenu = MenuPopupHelper(context as Activity, menuBuilder, image_filter!!)
        optionsMenu.setForceShowIcon(true)

        menuBuilder.setCallback(object : MenuBuilder.Callback {
            override fun onMenuItemSelected(menu: MenuBuilder, item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.id_sent -> {
                        presenter?.filterTweetList(item.titleCondensed.toString().toLowerCase())
                    }
                    R.id.id_popular -> {
                        presenter?.filterTweetList(item.titleCondensed.toString().toLowerCase())
                    }
                }
                return true
            }

            override fun onMenuModeChange(menu: MenuBuilder) {}
        })
        optionsMenu.show()
    }

    var presenter: HomeView.Presenter? = null
    var videoListAdapter: FilterItemListAdapter? = null
    var video_list: RecyclerView? = null
    var search_tweet: EditText? = null
    var image_filter: ImageView? = null
    var text_empty_view: TextView? = null
    var text_input: TextView? = null
    var image_twitter: ImageView? = null

    companion object {
        fun getInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onResume() {
        super.onResume()
        if (PrefManager.getInstance(context as Activity).getUserToken() != null) {
        } else {
            presenter?.getData(context as Activity)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_home, container, false)

        presenter = HomePresenter(this, context as Activity)
        presenter?.setView(rootView)

        search_tweet?.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    (presenter as HomePresenter)?.getFilterData(s.toString())
            }
        })
        return rootView
    }

    override fun setAdapterData(videoLists: ArrayList<Tweet>) {
        DialogUtil.stopProgressDisplay()

        if (videoLists != null && videoLists.size > 0) {
            videoListAdapter = FilterItemListAdapter(videoLists, getmActivity() as Activity)
            video_list!!.adapter = videoListAdapter
            videoListAdapter?.notifyDataSetChanged()

            video_list?.visibility = View.VISIBLE
            image_filter?.visibility = View.VISIBLE
            text_empty_view?.visibility = View.GONE
            image_twitter?.visibility = View.GONE
            text_input?.visibility = View.GONE
        } else {
            video_list?.visibility = View.GONE
            image_filter?.visibility = View.GONE
            text_empty_view?.visibility = View.VISIBLE
            image_twitter?.visibility = View.VISIBLE
            text_input?.visibility = View.VISIBLE
            text_empty_view?.setText(getString(R.string.no_tweets))
        }
    }

    override fun initViews(view: View) {
        text_empty_view = view.findViewById(R.id.text_empty_view)
        search_tweet = view.findViewById(R.id.search_tweet)
        text_input = view.findViewById(R.id.text_input)
        text_empty_view?.setText(getString(R.string.tweet_message))
        image_twitter = view.findViewById(R.id.image_twitter)
        image_filter = view.findViewById(R.id.image_filter)
        image_filter?.setOnClickListener(this)

        video_list = view.findViewById(R.id.faqList)
        video_list?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }
}