package com.blooms.twidapp.base

import android.view.View

interface BaseView<T> {
    abstract fun initViews(view: View)
}