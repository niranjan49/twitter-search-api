package com.niranjan.videoplayer.base

import android.app.Activity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.blooms.twidapp.listner.OnFragmentInteractionListener
import com.blooms.twidapp.utils.DialogUtil

open class BaseFragment : android.support.v4.app.Fragment() {
    internal var mListener: OnFragmentInteractionListener? = null
    protected var mActivity: AppCompatActivity? = null

    override fun onResume() {
        super.onResume()
        if (mActivity == null || activity == null || isDetached) {
            return
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            mActivity = context as AppCompatActivity
        }
        if (context is OnFragmentInteractionListener) run { mListener = context }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
        mActivity = null
    }

    fun getmActivity(): AppCompatActivity {
        return this.mActivity!!
    }
}