package com.niranjan.videoplayer.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import com.blooms.twidapp.listner.OnFragmentInteractionListener
import com.blooms.twidapp.utils.Constants
import com.example.twitter.twittersearch.R
import com.niranjan.videoplayer.base.BaseActivity
import com.example.twitter.twittersearch.ui.home.HomeFragment

class MainActivity : BaseActivity(), OnFragmentInteractionListener {
      internal var currentFragment: Int = 0
    internal var previousFragment: Fragment? = null
    internal var nextFragment: Fragment? = null

    override fun onFragmentInteraction(fragmentId: Int, data: Any?) {
        currentFragment = fragmentId
        val fragmentTag = fragmentId.toString()

        when (fragmentId) {
            Constants.FRAGMENT_HOME -> {
                navigateScreen(fragmentTag, HomeFragment.getInstance())
            }
        }
    }

    private fun navigateScreen(fragmentTag: String, currentFragment: Fragment) {
        navigateScreen(fragmentTag, currentFragment, true)
    }

    private fun navigateScreen(fragmentTag: String, currentFragment: Fragment, addToBackStack: Boolean) {
        nextFragment = currentFragment
        val fragmentManager = getSupportFragmentManager()
        val transaction = fragmentManager.beginTransaction()
        transaction.addToBackStack(fragmentTag)

        transaction.replace(R.id.fragment_main, currentFragment, fragmentTag).commit()
        previousFragment = nextFragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        onFragmentInteraction(Constants.FRAGMENT_HOME, null)
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (count <= 1) {
            closeApp()
        } else {
            super.onBackPressed()
        }
    }

    fun closeApp() {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setMessage(getString(R.string.exit_message))

        alertDialogBuilder.setPositiveButton(getString(R.string.yes)) { arg0, arg1 -> finish() }

        alertDialogBuilder.setNegativeButton(getString(R.string.no)) { dialog, which -> dialog.dismiss() }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }
}
