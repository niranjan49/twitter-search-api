package com.example.twitter.twittersearch.model

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class TweetsList{
    @SerializedName("statuses")
    var tweets: ArrayList<Tweet>? = null

}

