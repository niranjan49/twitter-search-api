package com.niranjan.videoplayer.network

import com.example.twitter.twittersearch.model.TweetsList
import com.example.twitter.twittersearch.model.TwitterToken
import com.example.twitter.twittersearch.network.ApiConstants
import retrofit2.Call
import retrofit2.http.*

interface ApiServices {

    companion object {
        val REQUEST_TWEET_LIST = 1
        val REQUEST_TWITTER_TOKEN = 2
        val REQUEST_POPULAR_TWEET_LIST = 3
    }

    @GET(ApiConstants.TWITTER_SEARCH_CODE)
    abstract fun getTweetList(@Header("Authorization") authorization: String,
                              @Query("q") hashtag: String, @Query("count") count: Int): Call<TweetsList>

    @FormUrlEncoded
    @POST("/oauth2/token")
    abstract fun getToken(
            @Header("Authorization") authorization: String,
            @Field("grant_type") grantType: String):
            Call<TwitterToken>

    @GET(ApiConstants.TWITTER_SEARCH_CODE)
    abstract fun getPopularTweetList(@Header("Authorization") authorization: String,
                                     @Query("q") hashtag: String, @Query("result_type") type: String,@Query("count") count: Int): Call<TweetsList>


}