package com.niranjan.videoplayer.network

interface EventListner {
    abstract fun onSuccessResponse(reqType: Int, data: Any)

    abstract fun onFailureResponse(reqType: Int, data: Any)
}