package com.blooms.twidapp.cache

import android.content.Context
import android.content.SharedPreferences
import com.example.twitter.twittersearch.model.TwitterToken
import com.google.gson.Gson

class PrefManager {

    val User_token = "user_token"

    fun setUserToken(token: TwitterToken) {
        editor.putString(User_token, Gson().toJson(token))
        editor.commit()
    }

    fun getUserToken(): TwitterToken? {
        return Gson().fromJson(pref.getString(User_token, null), TwitterToken::class.java)
    }

    companion object {

        private var instance: PrefManager? = null
        lateinit var pref: SharedPreferences
        lateinit var editor: SharedPreferences.Editor
        internal var PRIVATE_MODE = 0
        val PREF_NAME = "twitterDemoApp"

        fun setEditer(context: Context) {
            pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
            editor = pref.edit()
        }

        fun getInstance(ctx: Context): PrefManager {
            if (instance == null) instance = PrefManager()
            setEditer(ctx)

            return instance as PrefManager
        }

    }

}