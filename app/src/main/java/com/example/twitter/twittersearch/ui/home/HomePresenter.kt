package com.example.twitter.twittersearch.ui.home

import android.app.Activity
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.blooms.twidapp.utils.DialogUtil
import com.example.twitter.twittersearch.base.Presenter
import com.example.twitter.twittersearch.cache.AppCache
import com.example.twitter.twittersearch.model.Tweet
import com.example.twitter.twittersearch.model.TweetsList
import com.example.twitter.twittersearch.model.TwitterToken
import com.niranjan.videoplayer.network.ApiServices
import com.niranjan.videoplayer.network.EventListner
import com.niranjan.videoplayer.network.TransportManager

class HomePresenter(homeView: HomeView.View, context: Activity) : HomeView.Presenter, Presenter(), EventListner {

    override fun filterTweetList(resultType: String) {
        var filteredItems = ArrayList<Tweet>()
        val filterDta = AppCache.getCache().tweetList

        if (filterDta != null) {
            for (items in filterDta) {
                if (!TextUtils.isEmpty(items.metadata?.result_type) && items.metadata?.result_type!!.contains(resultType)) {
                    filteredItems?.add(items)
                }
            }

            if (filteredItems != null && filteredItems.size > 0) {
                view?.setAdapterData(filteredItems)
            } else {
                var token = getPref(context as Activity).getUserToken()!!.accessToken
                DialogUtil.displayProgress(context as Activity)
                    TransportManager.getInstance(this).getPopularTweetList(context as Activity, token as String, query!!, resultType)
            }
        }
    }

    var view: HomeView.View? = homeView
    var context: Activity? = context
    var rootView: View? = null
    var query: String? = null

    private fun initPresenter(rootView: View) {
        view?.initViews(rootView)
    }

    override fun getData(context: Activity) {
        DialogUtil.displayProgress(context)
        TransportManager.getInstance(this).onGetToken(context)
    }

    override fun onSuccessResponse(request: Int, data: Any) {
        DialogUtil.stopProgressDisplay()
        when (request) {
            ApiServices.REQUEST_TWITTER_TOKEN -> {

                var tokenData = data as TwitterToken
                getAppCache().token = tokenData.accessToken
                getPref(this.context!!).setUserToken(tokenData)
            }

            ApiServices.REQUEST_TWEET_LIST -> {
                val dataList = data as TweetsList
                getAppCache().tweetList = dataList.tweets!!
                view?.setAdapterData(dataList.tweets!!)
            }
            ApiServices.REQUEST_POPULAR_TWEET_LIST -> {
                val dataList = data as TweetsList
                view?.setAdapterData(dataList.tweets!!)
            }
        }
    }

    fun getFilterData(query: String?) {
        this?.query = query
        Log.d("? = null", query);
        var token = getPref(context as Activity).getUserToken()!!.accessToken

        if (token != null) {
            TransportManager.getInstance(this).getTweetList(context as Activity, token as String, query!!)
        } else {
            DialogUtil.displayProgress(context as Activity)
            TransportManager.getInstance(this).onGetToken(context as Activity)
        }
    }

    override fun onFailureResponse(reqType: Int, data: Any) {
        DialogUtil.stopProgressDisplay()
    }

    override fun setView(view: View) {
        rootView = view
        initPresenter(rootView!!)
    }

    override fun setActivity(activity: Activity) {
    }
}