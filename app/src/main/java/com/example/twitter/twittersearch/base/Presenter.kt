package com.example.twitter.twittersearch.base

import android.app.Activity
import com.blooms.twidapp.cache.PrefManager
import com.example.twitter.twittersearch.cache.AppCache

open class Presenter {
    fun getAppCache(): AppCache {
        return AppCache.getCache()
    }

    fun getPref(activity: Activity): PrefManager {
        return PrefManager.getInstance(activity)
    }
}