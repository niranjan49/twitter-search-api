package com.niranjan.videoplayer.network

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import com.blooms.twidapp.utils.DialogUtil
import com.blooms.twidapp.utils.Utils.Companion.getBase64String
import com.example.twitter.twittersearch.model.TweetsList
import com.example.twitter.twittersearch.model.TwitterToken
import com.example.twitter.twittersearch.network.ApiConstants
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Suppress("DEPRECATION")
class TransportManager {

    private var apiServices: ApiServices? = null
    internal lateinit var listener: EventListner

    companion object {
        var manager: TransportManager? = null
        fun getInstance(conlistener: EventListner): TransportManager {
            if (manager == null)
                manager = TransportManager()
            manager!!.setListener(conlistener)
            return manager as TransportManager
        }
    }

    fun setListener(listener: EventListner) {
        this.listener = listener
    }

    fun getAPIService(): ApiServices? {
        val gson = GsonBuilder()
                .setLenient()
                .create()

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val builder = OkHttpClient.Builder()
        builder.interceptors().add(interceptor)
        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.readTimeout(90, TimeUnit.SECONDS)

        val client = builder.build()
        val retrofit = Retrofit.Builder()
                .baseUrl(ApiConstants.TWITTER_SEARCH_BASRE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
        apiServices = retrofit.create(ApiServices::class.java)
        return apiServices
    }

    fun getTweetList(context: Context, twitterToken: String, query: String) {
        if (isConnectionAvailable(context)) {
            getAPIService()!!.getTweetList("Bearer " + twitterToken, query,ApiConstants.TWEET_COUNT).enqueue(object : Callback<TweetsList> {
                override fun onResponse(call: Call<TweetsList>, res: Response<TweetsList>) {
                    if (res.isSuccessful) {
                        listener.onSuccessResponse(ApiServices.REQUEST_TWEET_LIST, res.body()!!)
                    } else {
                        processResponse(res, ApiServices.REQUEST_TWEET_LIST)
                    }
                }

                override fun onFailure(call: Call<TweetsList>, arg0: Throwable) {
                    listener.onFailureResponse(ApiServices.REQUEST_TWEET_LIST, arg0.localizedMessage)
                }
            })
        } else {
            listener.onFailureResponse(ApiServices.REQUEST_TWEET_LIST, "NO_INTERNET")
        }
    }

    fun getPopularTweetList(context: Context, twitterToken: String, query: String, popular: String) {
        if (isConnectionAvailable(context)) {
            getAPIService()!!.getPopularTweetList("Bearer " + twitterToken, query,popular,ApiConstants.TWEET_COUNT).enqueue(object : Callback<TweetsList> {
                override fun onResponse(call: Call<TweetsList>, res: Response<TweetsList>) {
                    if (res.isSuccessful) {
                        listener.onSuccessResponse(ApiServices.REQUEST_POPULAR_TWEET_LIST, res.body()!!)
                    } else {
                        processResponse(res, ApiServices.REQUEST_POPULAR_TWEET_LIST)
                    }
                }

                override fun onFailure(call: Call<TweetsList>, arg0: Throwable) {
                    listener.onFailureResponse(ApiServices.REQUEST_POPULAR_TWEET_LIST, arg0.localizedMessage)
                }
            })
        } else {
            listener.onFailureResponse(ApiServices.REQUEST_POPULAR_TWEET_LIST, "NO_INTERNET")
        }
    }

    fun onGetToken(context: Context) {
        if (isConnectionAvailable(context)) {
            getAPIService()!!.getToken("Basic " + getBase64String(ApiConstants.BEARER_TOKEN_CREDENTIALS), "client_credentials").enqueue(object : Callback<TwitterToken> {
                override fun onResponse(call: Call<TwitterToken>, res: Response<TwitterToken>) {
                    if (res.isSuccessful) {
                        listener.onSuccessResponse(ApiServices.REQUEST_TWITTER_TOKEN, res.body()!!)
                    } else {
                        processResponse(res, ApiServices.REQUEST_TWITTER_TOKEN)
                    }
                }

                override fun onFailure(call: Call<TwitterToken>, arg0: Throwable) {
                    listener.onFailureResponse(ApiServices.REQUEST_TWITTER_TOKEN, arg0.localizedMessage)
                }
            })
        } else {
            listener.onFailureResponse(ApiServices.REQUEST_TWITTER_TOKEN, "NO_INTERNET")
        }
    }

    fun isConnectionAvailable(context: Context?): Boolean {
        if (context == null) return false
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    fun processResponse(res: Response<*>, reqType: Int) {
                DialogUtil.stopProgressDisplay()
    }
}

