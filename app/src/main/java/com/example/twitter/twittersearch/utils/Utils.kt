package com.blooms.twidapp.utils

import android.content.Context
import android.support.design.R.attr.height
import android.text.TextUtils
import android.util.Base64
import android.view.View
import android.widget.ImageView
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import java.io.UnsupportedEncodingException

class Utils {
    private val TAG = "MyFirebaseMsgService"

    companion object {
        @Throws(UnsupportedEncodingException::class)
        fun getBase64String(value: String): String {
            return Base64.encodeToString(value.toByteArray(charset("UTF-8")), Base64.NO_WRAP)
        }

        fun displayImage(imageView: ImageView, imageUrl: String, defaultImg: Int) {
            loadImage(imageView, imageUrl, defaultImg, -1, -1)
        }

        fun loadImage(imageView:ImageView, imageUrl:String, defaultImg:Int, width:Int, height:Int) {
            if (TextUtils.isEmpty(imageUrl)) {
                imageView.visibility=View.INVISIBLE
                return
            }
            val imageLoader = ImageLoader.getInstance()
            val options = DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisk(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(defaultImg)
                    .showImageOnFail(defaultImg)
                    .showImageOnLoading(defaultImg)
                    .build()

            if (width == -1 && height == -1) run { imageLoader.displayImage(imageUrl, imageView, options) }
        }

    }

}