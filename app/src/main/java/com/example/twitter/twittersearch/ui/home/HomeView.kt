package com.example.twitter.twittersearch.ui.home

import android.app.Activity
import com.blooms.twidapp.base.BasePresenter
import com.blooms.twidapp.base.BaseView
import com.example.twitter.twittersearch.model.Tweet
import java.util.*

interface HomeView {

    interface View : BaseView<Presenter> {
        fun setAdapterData(videoLists: ArrayList<Tweet>)
    }

    interface Presenter : BasePresenter {
        fun getData(context: Activity)
        fun filterTweetList(titleCondensed: String)
    }
}