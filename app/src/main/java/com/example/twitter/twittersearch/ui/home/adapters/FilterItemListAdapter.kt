package com.niranjan.videoplayer.ui.home.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.widget.TextView
import com.blooms.twidapp.utils.Utils
import com.example.twitter.twittersearch.R
import com.example.twitter.twittersearch.model.Tweet
import com.pkmmte.view.CircularImageView


class FilterItemListAdapter(items: ArrayList<Tweet>, activity: Context) :
        RecyclerView.Adapter<FilterItemListAdapter.ViewHolder>() {
    var context: Context? = activity
    var itemList: ArrayList<Tweet> = items

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FilterItemListAdapter.ViewHolder {
        val row = LayoutInflater.from(p0.getContext()).inflate(R.layout.tweet_list_item, p0, false)
        return ViewHolder(row);
    }

    override fun getItemCount(): Int {
        return if (itemList == null) 0 else itemList!!.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        val item = itemList!![p1]

        p0.text_name.setText(item.user?.name)
        p0.text_tweet.setText(item.text)
        p0.text_retweet.setText(item.retweet_count.toString())
        p0.text_fav.setText(item.favorite_count.toString())
        p0.text_screen_name.setText("@" + item.user?.screenName)

        if (item.user?.profile_image_url != null)
            Utils.displayImage( p0.profile_image, item.user?.profile_image_url!!, R.drawable.user)

        setFadeAnimation(p0?.container!!)
    }

    val FADE_DURATION = 500.00
    private fun setFadeAnimation(view: View) {
        val anim = AlphaAnimation(0.0f, 1.0f)
        anim.duration = FADE_DURATION.toLong()
        view.startAnimation(anim)
    }

    inner class ViewHolder(var mView: View) : RecyclerView.ViewHolder(mView) {

        internal var text_name: TextView
        internal var text_tweet: TextView
        internal var text_retweet: TextView
        internal var text_fav: TextView
        internal var text_screen_name: TextView
        internal var profile_image: CircularImageView
        internal var container: View? = null

        init {
            container = itemView.findViewById(R.id.layout_item)
            text_name = mView.findViewById(R.id.text_name)
            text_screen_name = mView.findViewById(R.id.text_screen_name)
            text_tweet = mView.findViewById(R.id.text_tweet)
            text_retweet = mView.findViewById(R.id.text_retweet)
            text_fav = mView.findViewById(R.id.text_fav)
            profile_image = mView.findViewById(R.id.profile_image)
        }
    }
}