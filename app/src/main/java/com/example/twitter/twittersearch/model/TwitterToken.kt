package com.example.twitter.twittersearch.model

import com.google.gson.annotations.SerializedName

class TwitterToken{

    @SerializedName("token_type")
    var tokenType: String? = null

    @SerializedName("access_token")
    var accessToken: String? = null
}