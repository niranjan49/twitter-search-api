package com.example.twitter.twittersearch.model

import com.google.gson.annotations.SerializedName

class TweetMetadata{

    @SerializedName("iso_language_code")
    var iso_language_code: String? = null

    @SerializedName("result_type")
    var result_type: String? = null


}