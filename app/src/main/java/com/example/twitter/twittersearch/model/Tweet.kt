package com.example.twitter.twittersearch.model

import com.google.gson.annotations.SerializedName

class Tweet {

    var favorite_count: Number? = 0
    var retweet_count: Number? = 0
    var name: String? = null

    @SerializedName("id")
    var id: String? = null

    @SerializedName("text")
    var text: String? = null

    @SerializedName("user")
    var user: TwitterUser? = null

    @SerializedName("metadata")
    var metadata: TweetMetadata? = null

    override fun toString(): String {
        return this!!.text!!
    }

}